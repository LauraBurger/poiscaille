import { createSlice } from '@reduxjs/toolkit';

export const formulasSlice = createSlice({
  name: 'formula',
  initialState: {
    name: null,
    price: null,
  },
  reducers: {
    setWeeklyFormula: (state) => {
      state.name = 'CHAQUE SEMAINE';
      state.price = 19.90;
    },
    setTwiceMonthFormula: (state) => {
      state.name = 'CHAQUE QUINZAINE';
      state.price = 22.90;
    },
    setMonthlyFormula: (state) => {
      state.name = 'CHAQUE MOIS';
      state.price = 24.90;
    },
  },
});

export const { setWeeklyFormula, setTwiceMonthFormula, setMonthlyFormula } = formulasSlice.actions;

export default formulasSlice.reducer;
