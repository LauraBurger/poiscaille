import React, { useState, useCallback } from 'react';
import Grid from '@mui/material/Grid';
import AddIcon from '@mui/icons-material/Add';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Fab from '@mui/material/Fab';
import RemoveIcon from '@mui/icons-material/Remove';
import { styled } from '@mui/material/styles';
import parse from 'html-react-parser';

import { useSelector, useDispatch } from 'react-redux';
import bespoke from '../assets/locker-bespoke.svg';
import choice from '../assets/locker-choice.svg';
import flexible from '../assets/locker-star.svg';

import Prices from '../composants/Prices';
import Footer from '../composants/Footer';
import SwipeableTextMobileStepper from '../composants/SwipeableTextMobileStepper';
import LeafletMap from '../composants/LeafletMap';
import TopBar from '../composants/TopBar';
import Pillars from '../composants/Pillars';
import Section from '../composants/Section';
import { decrement, increment } from '../features/counter/counterSlice';
import { setWeeklyFormula, setTwiceMonthFormula, setMonthlyFormula } from '../features/counter/formula';

import '../styles/Section.css';

const SelectionButton = styled(Button)(() => ({
  fontSize: 12,
  fontFamily: 'Lato',
  fontWeight: '400',
  backgroundColor: '#fee575',
  color: '#424242',
  height: 36,
  width: 200,
  paddingLeft: '10px',
  paddingRight: '10px',
  paddingTop: '4px',
  paddingBottom: '4px',
  display: 'block',
  margin: 'auto',
  '&:hover': {
    backgroundColor: '#fee575',
  },
}));

const SelectionBox = styled(Box)(() => ({
  paddingTop: 12,
  paddingBottom: 12,
  paddingRight: 12,
  paddingLeft: 12,
  textAlign: 'center',
  height: 151,
  width: 288,
}));

const salesArguments = [
  {
    picture: bespoke,
    alt: 'Bespoke',
    name: 'bespoke-picture',
    title: 'MA FORMULE SUR MESURE',
    text: 'Je choisis la quantité qu’il me faut à la fréquence qui me convient. Je renseigne mes allergies ou aversions.',
  },
  {
    picture: choice,
    alt: 'Choice',
    name: 'choice-picture',
    title: 'AU CHOIX PARMI LA PÊCHE DU JOUR',
    text: 'La veille de chaque casier, un email m’annonce les différentes combinaisons de produits de la pêche du jour. J’indique ma préférence ou me laisse surprendre.',
  },
  {
    picture: flexible,
    alt: 'Flexible',
    name: 'flexible-picture',
    title: 'FLEXIBLE ET SANS ENGAGEMENT',
    text: "Jusqu'à 3 jours avant chaque casier (ou 4 en région), je peux le reporter à plus tard, changer de lieu de livraison, ou tout arrêter en un clic.",
  },
];

const formulas = [
  {
    frequency: 'CHAQUE SEMAINE',
    amount: '19.90€/casier ex. tous les samedis',
    label: 'weekly formula value',
    select: 'Choisir',
    onClick: setWeeklyFormula,
  },
  {
    frequency: 'CHAQUE QUINZAINE',
    amount: '22.90€/casier ex. tous les samedis',
    label: 'twice month formula value',
    select: 'Choisir',
    onClick: setTwiceMonthFormula,
  },
  {
    frequency: 'CHAQUE MOIS',
    amount: '24.90€/casier ex. tous les samedis',
    label: 'monthly month formula value',
    select: 'Choisir',
    onClick: setMonthlyFormula,
  },
];

function getOrderDetail(locker) {
  return `Un repas pour ${2 * locker} - ${3 * locker} personnes.<br/>
        <strong>${1 * locker} kg </strong> de poisson<br/>
        ou<br/>
        <strong>${2 * locker} kg</strong> de coquillages<br/>
        ou<br/>
        <strong>${0.5 * locker} kg</strong> de poisson + <strong>${1 * locker} kg</strong> de coquillages`;
}

function Shop() {
  const count = useSelector((state) => state.counter.value);
  const formulaName = useSelector((state) => state.formula.name);
  const formulaPrice = useSelector((state) => state.formula.price);
  const dispatch = useDispatch();

  const [setState] = useState({
    location: '',
  });

  const onSubmit = useCallback((event) => {
    event.preventDefault();
    setState(
      (prevState) => ({
        ...prevState,
        location: event.target.location.value,
      }),
    );
  }, [setState]);

  return (
    <>
      <TopBar />
      <Section title="Le casier de la mer" text="Votre dose de produits de la mer ultra frais, sans contrainte, en direct des pêcheurs. Vous choisissez la fréquence, la quantité et le lieu de livraison. On se charge du reste." />
      <Grid
        container
        item
        margin="0px"
        padding="48px 24px 24px 24px"
        justifyContent="center"
        sx={{ mb: 6 }}
      >
        <Pillars data={salesArguments} />
      </Grid>
      <SwipeableTextMobileStepper />

      <Section
        padding="48px 24px"
        title="Un peu, beaucoup, à la folie"
        text="À petite dose ou sans compter, pour une petite ou une grande famille. Je choisis la quantité et la fréquence auxquelles recevoir mes produits de la mer. Un casier nourrit 2 à 3 personnes."
      />
      <Grid
        container
        item
        margin="0px"
        padding="48px 24px"
        justifyContent="center"
      >
        <Prices data={formulas} />
      </Grid>

      <Section title="Ma quantité idéale" text="Livraison gratuite en point relais — offerte partout à domicile en France dès 4 casiers. Pas de frais de livraison en supplément, que je prenne un casier simple, double ou plus encore." />
      <Grid
        container
        item
        margin="0px"
        padding="24px"
        justifyContent="center"
      >
        <SelectionBox>
          <div className="div-quantity">
            <Fab
              size="small"
              color="grey.200"
              aria-label="Decrement value"
              onClick={() => dispatch(decrement())}
            >
              <RemoveIcon />
            </Fab>
            <p className="body-quantity">
              <strong>{count}</strong>
              {' '}
              casier
              {count > 1 ? 's' : ''}
              {' '}
              {formulaName}
              {formulaPrice}
            </p>
            <Fab
              size="small"
              color="grey.200"
              aria-label="Increment value"
              onClick={() => dispatch(increment())}
            >
              <AddIcon />
            </Fab>
          </div>
          <SelectionButton sx={{ mt: 2 }}>
            OK pour
            {' '}
            {count}
            {' '}
            casier
            {count > 1 ? 's' : ''}
          </SelectionButton>
        </SelectionBox>
        <SelectionBox>
          <p className="body-description">{parse(getOrderDetail(count))}</p>
        </SelectionBox>
      </Grid>

      <Box sx={{ mt: 10 }}>
        <Section title="Où récupérer mon casier" />
        <LeafletMap handleSubmit={onSubmit} />
      </Box>
      <Footer />
    </>
  );
}

export default Shop;
