import Grid from '@mui/material/Grid';
import React, { useState, useCallback } from 'react';
import Button from '../composants/Button';

import TopBar from '../composants/TopBar';
import BackgroundImage from '../composants/BackgroundImage';
import Pillars from '../composants/Pillars';
import Section from '../composants/Section';
import Footer from '../composants/Footer';
import Pillar from '../composants/Pillar';
import SwipeableTextMobileStepper from '../composants/SwipeableTextMobileStepper';

import savage from '../assets/home-savage.svg';
import enduring from '../assets/home-enduring.svg';
import fresh from '../assets/home-fresh.svg';
import ethics from '../assets/home-ethics.svg';
import delivery from '../assets/home-delivery.svg';
import fishing from '../assets/home-fishing.svg';
import formula from '../assets/home-formula.svg';
import lockers from '../assets/home-lockers.svg';
import star from '../assets/home-star.svg';

const values = [
  {
    picture: savage,
    className: 'value',
    alt: 'Savage',
    title: 'SAUVAGE, 100% FRANÇAIS',
    text: 'Nos prises sont attrapées en France en milieu naturel. Pas d’élevage, pas de chimie.',
  },
  {
    picture: enduring,
    className: 'value',
    alt: 'Enduring',
    title: 'UNE PÊCHE DURABLE',
    text: 'En direct de pêcheurs aux pratiques vertueuses soucieux de leur impact sur les océans.',
  },
  {
    picture: fresh,
    className: 'value',
    alt: 'Fresh',
    title: 'FRAIS COMME À LA CÔTE',
    text: '48 heures maximum entre la pêche à bord du bateau et votre assiette.',
  },
  {
    picture: ethics,
    className: 'value',
    alt: 'Ethics',
    title: 'ÉTHIQUE',
    text: 'Espèces nobles comme oubliées sont valorisées, les pêcheurs sont payés à prix juste.',
  },
];

const steps = [
  {
    picture: formula,
    className: 'step',
    alt: 'Formula',
    title: 'JE CRÉE MA FORMULE',
    text: 'Pour recevoir la quantité qu’il me faut, à la fréquence qui me plait. Je précise mes préférences ou allergies.',
  },
  {
    picture: fishing,
    className: 'step',
    alt: 'Fishing',
    title: 'LES PÊCHEURS PÊCHENT',
    text: "Toutes les espèces sont valorisées. Chaque jour, c'est la mer qui décide ce qu'il y aura dans vos casiers !",
  },
  {
    picture: lockers,
    className: 'step',
    alt: 'Lockers',
    title: 'JE CHOISIS MON CASIER',
    text: 'Parmi les 3 à 6 combinaisons de produits disponibles, composées en fonction de la pêche du jour.',
  },
  {
    picture: delivery,
    className: 'step',
    alt: 'Delivery',
    title: 'JE REÇOIS MON COLIS',
    text: 'Expédié en express via camion frigo, il arrive chez un commerçant relais ou à domicile dans toute la France.',
  },
  {
    picture: star,
    className: 'step',
    alt: 'Star',
    title: 'MA FORMULE EST FLEXIBLE',
    text: 'Absent un weekend, ou plusieurs semaines, je reporte mon casier à plus tard. J’arrête quand je veux.',
  },
];

const pillars = values.map((pillar) => (
  <Grid item xs={6} md={3} key={pillar.picture}>
    <Pillar
      src={pillar.picture}
      alt={pillar.alt}
      className={pillar.className}
      title={pillar.title}
      text={pillar.text}
    />
  </Grid>
));

function HomePage() {
  const [setState] = useState({
    email: '',
  });

  const onSubmit = useCallback((event) => {
    event.preventDefault();
    setState(
      (prevState) => ({
        ...prevState,
        email: event.target.email.value,
      }),
    );
  }, [setState]);

  return (
    <>
      <TopBar />
      <BackgroundImage />

      <Grid
        container
        item
        margin="0px"
        padding="48px 24px 24px 24px"
        justifyContent="center"
      >
        {pillars}
      </Grid>

      <Section title="Le casier de la mer" text="La version marine du panier de légumes, flexible et sans engagement. Abonnez-vous et recevez poissons, coquillages et crustacés, en fonction de la pêche du jour." />
      <SwipeableTextMobileStepper />

      <Grid
        container
        item
        margin="0px"
        padding="48px 24px"
        justifyContent="center"
      >
        <Pillars data={steps} />
      </Grid>
      <Button variant="text" className="large">COOL, JE M&apos;ABONNE</Button>
      <Footer emailSubmit={onSubmit} />
    </>
  );
}

export default HomePage;
