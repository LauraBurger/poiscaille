import React, { useState, useCallback } from 'react';
import Forms from '../composants/Forms';

function Subscription() {
  const [state, setState] = useState({
    email: '',
    mobile: '',
    firstname: '',
    name: '',
    password: '',
  });

  const onSubmit = useCallback((event) => {
    event.preventDefault();
    setState(
      (prevState) => ({
        ...prevState,
        email: event.target.email.value,
        mobile: event.target.mobile.value,
        firstname: event.target.firstname.value,
        name: event.target.name.value,
        password: event.target.password.value,
      }),
    );
  }, [setState]);

  return (
    <Forms handleSubmit={onSubmit} />
  );
}

export default Subscription;
