import { configureStore } from '@reduxjs/toolkit';
import counterReducer from '../features/counter/counterSlice';
import formulasReducer from '../features/counter/formula';

export default configureStore({
  reducer: {
    counter: counterReducer,
    formula: formulasReducer,
  },
});
