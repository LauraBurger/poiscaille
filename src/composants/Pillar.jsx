import React from 'react';
import Box from '@mui/material/Box';
import { styled } from '@mui/material/styles';
import PropTypes from 'prop-types';
import '../styles/Pillar.css';

const PillarBox = styled(Box)(() => ({
  textAlign: 'center',
  minHeight: 100,
  width: 199,
}));

function Pillar({
  src, alt, className, title, text,
}) {
  return (
    <PillarBox className="pillar">
      <img src={src} alt={alt} className={className} />
      <h4>{title}</h4>
      <p>{text}</p>
    </PillarBox>
  );
}

Pillar.propTypes = {
  src: PropTypes.string.isRequired,
  alt: PropTypes.string.isRequired,
  className: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
};

export default Pillar;
