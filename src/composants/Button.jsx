import MaterialButton from '@mui/material/Button';
import { styled } from '@mui/material/styles';

const Button = styled(MaterialButton)(() => ({
  className: 'button',
  fontSize: 14,
  fontFamily: 'Lato',
  fontWeight: '400',
  backgroundColor: '#fee575',
  color: '#424242',
  height: 43,
  padding: '4px 10 px 4px 10px',
  display: 'block',
  margin: 'auto',
  '&:hover': {
    backgroundColor: '#fee575',
  },
}));

export default Button;
