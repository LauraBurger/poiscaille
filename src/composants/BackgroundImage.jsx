import React from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import { styled } from '@mui/material/styles';
import background from '../assets/backgroundimg.jpg';
import '../styles/BackgroundImage.css';

const DiscoverButton = styled(Button)(() => ({
  fontSize: 14,
  fontFamily: 'Lato',
  fontWeight: '400',
  backgroundColor: '#fee575',
  color: '#424242',
  height: 43,
  width: 250,
  padding: ' 4px 10px 4px 10px',
  display: 'block',
  margin: 'auto',
  '&:hover': {
    backgroundColor: '#fee575',
  },
}));

function BackgroundImage() {
  return (
    <Grid container>
      <Grid
        item
        xs={12}
        sm={12}
        md={12}
        sx={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'flex-end',
        }}
      >
        <div style={{
          backgroundImage: `url(${background})`,
          height: '291px',
          backgroundSize: 'cover',
          width: '100%',
        }}
        >
          <Box>
            <h2>LE CIRCUIT COURT DES PRODUITS DE LA MER</h2>
            <h1>
              Retrouvez le
              {' '}
              <span className="taste">goût</span>
              {' '}
              du poisson
            </h1>
          </Box>
          <DiscoverButton variant="text">DECOUVRIR NOS FORMULES</DiscoverButton>
        </div>
      </Grid>
    </Grid>
  );
}

export default BackgroundImage;
