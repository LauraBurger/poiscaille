import AppBar from '@mui/material/AppBar';
import { styled } from '@mui/material/styles';
import React from 'react';
import '../styles/TopBar.css';
import Toolbar from '@mui/material/Toolbar';
import Box from '@mui/material/Box';
import { Link } from 'react-router-dom';
import Button from '@mui/material/Button';
import logo from '../assets/logo.png';

const TopBar = styled(AppBar)(() => ({
  height: 66,
  boxShadow: '0px 0px 0px 0px',
  position: 'sticky',
  background: '#ffffff',
}));

const TopBarButton = styled(Button)(() => ({
  fontSize: 12,
  fontFamily: 'Lato',
  backgroundColor: '#ffffff',
  color: '#4A4A4A',
  height: 66,
  paddingLeft: '16px',
  paddingRight: '16px',
  paddingTop: '8px',
  paddingBottom: '8px',
  '&:hover': {
    backgroundColor: '#eeeeee',
  },
}));

const TopBarButtonSubscribe = styled(Button)(() => ({
  fontSize: 12,
  fontFamily: 'Lato',
  backgroundColor: '#fee575',
  color: '#424242',
  height: 47,
  paddingLeft: '16px',
  paddingRight: '16px',
  paddingTop: '8px',
  paddingBottom: '8px',
  '&:hover': {
    backgroundColor: '#fee575',
  },
}));

function Bar() {
  return (
    <TopBar position="static">
      <Toolbar>
        <Link to="/"><img src={logo} alt="Poiscaille" className="poiscaille-logo" /></Link>
        <Box
          sx={{
            display: 'flex',
            justifyContent: 'flex-end',
            alignItems: 'center',
            width: '100%',
          }}
        >
          <TopBarButton variant="text">ON RECRUTE</TopBarButton>
          <TopBarButton variant="text">PECHEURS</TopBarButton>
          <TopBarButton variant="text">RECETTES</TopBarButton>
          <TopBarButton variant="text">
            <Link to="/shop">BOUTIQUE</Link>
          </TopBarButton>
          <TopBarButton variant="text">MON COMPTE</TopBarButton>
          <TopBarButtonSubscribe variant="text">S&apos;ABONNER</TopBarButtonSubscribe>
        </Box>
      </Toolbar>
    </TopBar>
  );
}

export default Bar;
