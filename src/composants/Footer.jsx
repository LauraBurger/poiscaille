import React from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import FacebookIcon from '@mui/icons-material/Facebook';
import InstagramIcon from '@mui/icons-material/Instagram';
import TwitterIcon from '@mui/icons-material/Twitter';
import YouTubeIcon from '@mui/icons-material/YouTube';
import Link from '@mui/material/Link';
import { TextField } from '@mui/material';
import { styled } from '@mui/material/styles';
import '../styles/Footer.css';
import PropTypes from 'prop-types';

const FooterBox = styled(Box)(() => ({
  paddingTop: 12,
  paddingBottom: 12,
  paddingRight: 12,
  paddingLeft: 12,
  display: 'flex',
  flexDirection: 'column',
  width: 240,
}));

const IconBox = styled(Box)(() => ({
  display: 'flex',
  flexDirection: 'row',
}));

const NewsletterButton = styled(Button)(() => ({
  width: '10px',
  color: '#ffe575',
  backgroundColor: 'black',
  border: 'none',
  '&:hover': {
    backgroundColor: 'black',
    border: 'none',
  },
}));

const FooterTextField = styled(TextField)(() => ({
  width: '150px',
  '& label.Mui-focused': {
    color: 'white',
  },
  '& .MuiOutlinedInput-root': {
    '&.Mui-focused fieldset': {
      borderColor: 'black',
    },
  },
}));

const FooterLink = styled(Link)(() => ({
  fontSize: 13,
  fontFamily: 'Lato',
  fontWeight: '400',
  backgroundColor: '#ffe575',
  color: '#4a4a4a',
  textAlign: 'left',
  marginBottom: '8px',
}));

function Footer({ emailSubmit }) {
  return (
    <Grid
      container
      className="footer-grid"
      sx={{ mt: 6 }}
    >
      <Grid
        container
        item
        marginLeft="0px"
        marginTop="0px"
        justifyContent="center"
      >
        <Grid item xs={6} md={3}>
          <FooterBox>
            <h4 className="title-footer">POISCAILLE</h4>
            <FooterLink href="#" underline="none">Pêcheurs</FooterLink>
            <FooterLink href="#" underline="none">Recettes</FooterLink>
            <FooterLink href="#" underline="none">Conservation</FooterLink>
            <FooterLink href="#" underline="none">Blog</FooterLink>
          </FooterBox>
        </Grid>

        <Grid item xs={6} md={3}>
          <FooterBox>
            <h4>A PROPOS</h4>
            <FooterLink href="#" underline="none">Boutique</FooterLink>
            <FooterLink href="#" underline="none">S&apos;abonner</FooterLink>
            <FooterLink href="#" underline="none">Mon compte</FooterLink>
            <FooterLink href="#" underline="none">Aide et contact</FooterLink>
            <FooterLink href="#" underline="none">Qui sommes-nous</FooterLink>
            <FooterLink href="#" underline="none">Mentions légales</FooterLink>
            <FooterLink href="#" underline="none">CGV</FooterLink>
          </FooterBox>
        </Grid>

        <Grid item xs={6} md={3}>
          <FooterBox>
            <h4>SUIVEZ-NOUS</h4>
            <IconBox>
              <FacebookIcon fontSize="large" sx={{ mr: 1 }} />
              <InstagramIcon fontSize="large" sx={{ mr: 1 }} />
              <TwitterIcon fontSize="large" sx={{ mr: 1 }} />
              <YouTubeIcon fontSize="large" />
            </IconBox>
          </FooterBox>
        </Grid>

        <Grid item xs={6} md={3}>
          <FooterBox>
            <h4>NEWSLETTER</h4>
            <form
              onSubmit={emailSubmit}
              row="true"
            >
              <FooterTextField
                inputProps={{ style: { fontSize: 13 } }}
                placeholder="Votre email"
                size="small"
                name="email"
                sx={{ mr: 1 }}
              />
              <NewsletterButton variant="outlined" type="submit">
                OK
              </NewsletterButton>
            </form>
          </FooterBox>
        </Grid>
      </Grid>
    </Grid>
  );
}

Footer.propTypes = {
  emailSubmit: PropTypes.func.isRequired,
};

export default Footer;
