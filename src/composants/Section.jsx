import React from 'react';
import PropTypes from 'prop-types';
import '../styles/Section.css';

function Section({ title, text }) {
  return (
    <div className="section">
      <h3>{title}</h3>
      <div className="yellow-line" />
      <p className="text-h3">{text}</p>
    </div>
  );
}

Section.propTypes = {
  title: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
};

export default Section;
