import React from 'react';
import { TextField } from '@mui/material';
import { styled } from '@mui/material/styles';
import Button from '@mui/material/Button';
import PropTypes from 'prop-types';

import lifebuoy from '../assets/user-lifebuoy.svg';
import '../styles/Forms.css';

const FormTextField = styled(TextField)(() => ({
  width: '150px',
  '& label.Mui-focused': {
    color: 'white',
  },
  '& .MuiOutlinedInput-root': {
    '&.Mui-focused fieldset': {
      borderColor: 'black',
    },
  },
}));

const FormButton = styled(Button)(() => ({
  width: '90px',
  color: '#ffe575',
  backgroundColor: 'black',
  border: 'none',
  '&:hover': {
    backgroundColor: 'black',
    border: 'none',
  },
}));

function Forms({ handleSubmit }) {
  return (
    <form
      onSubmit={handleSubmit}
      row="true"
    >
      <img src={lifebuoy} alt="lifebuoy" className="lifebuoy" />
      <h1>NOUVEAU COMPTE</h1>
      <p>
        Bonjour moussaillon, venez prendre le large à nos côtés,
        vous verrez, ça mord dans le coin.
      </p>

      <FormTextField
        inputProps={{ style: { fontSize: 13 } }}
        placeholder="EMAIL"
        size="small"
        name="email"
      />

      <FormTextField
        inputProps={{ style: { fontSize: 13 } }}
        placeholder="TELEPHONE MOBILE"
        size="small"
        name="mobile"
      />

      <FormTextField
        inputProps={{ style: { fontSize: 13 } }}
        placeholder="PRENOM"
        size="small"
        name="firstname"
      />

      <FormTextField
        inputProps={{ style: { fontSize: 13 } }}
        placeholder="NOM"
        size="small"
        name="name"
      />

      <FormTextField
        inputProps={{ style: { fontSize: 13 } }}
        placeholder="MOT DE PASSE"
        size="small"
        name="password"
      />

      <FormButton variant="outlined" type="submit">
        CREER MON COMPTE
      </FormButton>
    </form>
  );
}

Forms.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
};

export default Forms;
