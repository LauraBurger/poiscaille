import React, { useState } from 'react';
import Price from './Price';

function Prices(props) {
  const [frequency, setFrequency] = useState('');

  function onClick() {
    setFrequency(frequency);
  }

  const prices = props.data.map((price) => (
    <Price
      key={price.frequency}
      frequency={price.frequency}
      amount={price.amount}
      label={price.label}
      onClick={() => {
        price.onClick();
        onClick();
      }}
      select={price.select}
    />
  ));
  return (
    prices
  );
}

export default Prices;
