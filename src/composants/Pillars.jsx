import React from 'react';
import Pillar from './Pillar';

function Pillars({ data }) {
  const pillars = data.map((pillar) => (
    <Pillar
      key={pillar.picture}
      src={pillar.picture}
      alt={pillar.alt}
      className={pillar.className}
      title={pillar.title}
      text={pillar.text}
    />
  ));

  return (
    pillars
  );
}

export default Pillars;
