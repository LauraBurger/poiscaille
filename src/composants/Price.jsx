import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import { styled } from '@mui/material/styles';
import React from 'react';
import PropTypes from 'prop-types';

const PriceBox = styled(Box)(() => ({
  padding: 12,
  textAlign: 'center',
  height: 190,
  width: 190,
  margin: 'auto',
}));

const OrderButton = styled(Button)(() => ({
  fontSize: 14,
  fontFamily: 'Lato',
  fontWeight: '400',
  backgroundColor: '#fee575',
  color: '#424242',
  height: 36,
  width: 150,
  padding: '4px 10px 4px 10px',
  display: 'block',
  margin: 'auto',
  '&:hover': {
    backgroundColor: '#fee575',
  },
}));

function Price({
  frequency, amount, label, onClick, select,
}) {
  return (
    <PriceBox sx={{ border: 3, borderColor: 'grey.200' }}>
      <h4>{frequency}</h4>
      <p>{amount}</p>
      <OrderButton
        aria-label={label}
        onClick={onClick}
      >
        {select}
      </OrderButton>
    </PriceBox>
  );
}

Price.propTypes = {
  frequency: PropTypes.string.isRequired,
  amount: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  select: PropTypes.string.isRequired,
};

export default Price;
