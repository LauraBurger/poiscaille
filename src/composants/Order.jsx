import AddIcon from '@mui/icons-material/Add';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Fab from '@mui/material/Fab';
import Grid from '@mui/material/Grid';
import RemoveIcon from '@mui/icons-material/Remove';
import { styled } from '@mui/material/styles';
import { useSelector, useDispatch } from 'react-redux';
import parse from 'html-react-parser';
import SwipeableTextMobileStepper from './SwipeableTextMobileStepper';
import Pillars from './Pillars';

import bespoke from '../assets/locker-bespoke.svg';
import choice from '../assets/locker-choice.svg';
import flexible from '../assets/locker-star.svg';

import { decrement, increment } from '../features/counter/counterSlice';
import { setWeeklyFormula, setTwiceMonthFormula, setMonthlyFormula } from '../features/counter/formula';

import '../styles/Carousel.css';
// import '../styles/Order.css'

const OrderButton = styled(Button)(({ theme }) => ({
  fontSize: 14,
  fontFamily: 'Lato',
  fontWeight: '400',
  backgroundColor: '#fee575',
  color: '#424242',
  height: 36,
  width: 150,
  padding: '4px 10px 4px 10px',
  display: 'block',
  margin: 'auto',
  '&:hover': {
    backgroundColor: '#fee575',
	  },
}));

const SelectionButton = styled(Button)(({ theme }) => ({
  fontSize: 12,
  fontFamily: 'Lato',
  fontWeight: '400',
  backgroundColor: '#fee575',
  color: '#424242',
  height: 36,
  width: 200,
  paddingLeft: '10px',
  paddingRight: '10px',
  paddingTop: '4px',
  paddingBottom: '4px',
  display: 'block',
  margin: 'auto',
  '&:hover': {
    backgroundColor: '#fee575',
	  },
}));

const SelectionBox = styled(Box)(({ theme }) => ({
  paddingTop: 12,
  paddingBottom: 12,
  paddingRight: 12,
  paddingLeft: 12,
  textAlign: 'center',
  height: 151,
  width: 288,
}));

const PriceBox = styled(Box)(({ theme }) => ({
  paddingTop: 12,
  paddingBottom: 12,
  paddingRight: 12,
  paddingLeft: 12,
  textAlign: 'center',
  height: 190,
  width: 190,
  margin: 'auto',
}));

const salesArguments = [
  {
    picture: bespoke,
    alt: 'Bespoke',
    name: 'bespoke-picture',
    title: 'MA FORMULE SUR MESURE',
    text: 'Je choisis la quantité qu’il me faut à la fréquence qui me convient. Je renseigne mes allergies ou aversions.',
  },
  {
    picture: choice,
    alt: 'Choice',
    name: 'choice-picture',
    title: 'AU CHOIX PARMI LA PÊCHE DU JOUR',
    text: 'La veille de chaque casier, un email m’annonce les différentes combinaisons de produits de la pêche du jour. J’indique ma préférence ou me laisse surprendre.',
  },
  {
    picture: flexible,
    alt: 'Flexible',
    name: 'flexible-picture',
    title: 'FLEXIBLE ET SANS ENGAGEMENT',
    text: "Jusqu'à 3 jours avant chaque casier (ou 4 en région), je peux le reporter à plus tard, changer de lieu de livraison, ou tout arrêter en un clic.",
  },
];

const formulas = [
  {
    frequency: 'CHAQUE SEMAINE',
    price: '19.90€/casier ex. tous les samedis',
    label: 'weekly formula value',
    select: 'Choisir',
    onClick: setWeeklyFormula,
  },
  {
    frequency: 'CHAQUE QUINZAINE',
    price: '22.90€/casier ex. tous les samedis',
    label: 'twice month formula value',
    select: 'Choisir',
    onClick: setTwiceMonthFormula,
  },
  {
    frequency: 'CHAQUE MOIS',
    price: '24.90€/casier ex. tous les samedis',
    label: 'monthly month formula value',
    select: 'Choisir',
    onClick: setMonthlyFormula,
  },
];

function Order() {
  const count = useSelector((state) => state.counter.value);
  const formulaName = useSelector((state) => state.formula.name);
  const formulaPrice = useSelector((state) => state.formula.price);
  const dispatch = useDispatch();

  const componentFormulas = formulas.map((formula) => (
    <PriceBox sx={{ border: 3, borderColor: 'grey.200' }}>
      <h4>{formula.frequency}</h4>
      <p>{formula.price}</p>
      <OrderButton
        aria-label={formula.label}
        onClick={() => dispatch(formula.onClick())}
      >
        {formula.select}
      </OrderButton>
    </PriceBox>
  ));

  function getOrderDetail(locker) {
    return `Un repas pour ${2 * locker} - ${3 * locker} personnes.<br/>
        <strong>${1 * locker} kg </strong> de poisson<br/>
        ou<br/>
        <strong>${2 * locker} kg</strong> de coquillages<br/>
        ou<br/>
        <strong>${0.5 * locker} kg</strong> de poisson + <strong>${1 * locker} kg</strong> de coquillages`;
  }

  return (
    <Grid
      container
    >
      <Grid
        item
        xs={12}
        sm={12}
        md={12}
        sx={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <Box>
          <h3>Un peu, beaucoup, à la folie</h3>
          <div className="yellow-line" />
          <p className="text-h3">
            À petite dose ou sans compter, pour une petite ou une grande famille.
            <br />
            Je choisis la quantité et la fréquence auxquelles recevoir mes produits de la mer.
            <br />
            Un casier nourrit 2 à 3 personnes.
          </p>
        </Box>
      </Grid>

      <Grid
        container
        item
        margin="0px"
        paddingTop="48px 24px 24px 24px"
        justifyContent="center"
      >
        <Pillars data={salesArguments} />
      </Grid>

      <SwipeableTextMobileStepper />

      <Grid
        item
        xs={12}
        sm={12}
        md={12}
        sx={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <Box>
          <h3>Un peu, beaucoup, à la folie</h3>
          <div className="yellow-line" />
          <p className="text-h3">
            À petite dose ou sans compter, pour une petite ou une grande famille.
            <br />
            Je choisis la quantité et la fréquence auxquelles recevoir mes produits de la mer.
            <br />
            Un casier nourrit 2 à 3 personnes.
          </p>
        </Box>
      </Grid>

      <Grid
        container
        item
        margin="0px"
        paddingTop="48px 24px 24px 24px"
        justifyContent="center"
      >
        {componentFormulas}
      </Grid>

      <Grid
        item
        xs={12}
        sm={12}
        md={12}
        sx={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <Box>
          <h3>Ma quantité idéale</h3>
          <div className="yellow-line" />
          <p className="text-h3">
            Livraison gratuite en point relais — offerte partout à domicile en France dès 4 casiers.
            <br />
            Pas de frais de livraison en supplément, que je prenne un casier simple, double ou plus encore.
          </p>
        </Box>
      </Grid>

      <Grid
        container
        item
        margin="0px"
        paddingTop="48px 24px 24px 24px"
        justifyContent="center"
      >
        <SelectionBox>
          <div className="div-quantity">
            <Fab
              size="small"
              color="grey.200"
              aria-label="Decrement value"
              onClick={() => dispatch(decrement())}
            >
              <RemoveIcon />
            </Fab>
            <p className="body-quantity">
              <strong>{count}</strong>
              {' '}
              casier
              {count > 1 ? 's' : ''}
              {' '}
              {formulaName}
              {formulaPrice}
            </p>
            <Fab
              size="small"
              color="grey.200"
              aria-label="Increment value"
              onClick={() => dispatch(increment())}
            >
              <AddIcon />
            </Fab>
          </div>
          <SelectionButton>
            OK pour
            {' '}
            {count}
            {' '}
            casier
            {count > 1 ? 's' : ''}
          </SelectionButton>
        </SelectionBox>
        <SelectionBox>
          <p className="body-description">{parse(getOrderDetail(count))}</p>
        </SelectionBox>
      </Grid>
    </Grid>
  );
}

export default Order;
