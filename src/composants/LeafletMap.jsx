import {
  MapContainer, TileLayer, Marker, Popup,
} from 'react-leaflet';
import 'leaflet/dist/leaflet.css';
import '../styles/LeafletMap.css';
import { Icon } from 'leaflet';
import React, { useState, useCallback } from 'react';
import marker from '../assets/marker.png';

const myIcon = new Icon({
  iconUrl: marker,
  iconSize: [32, 32],
});

function LeafletMap() {
  const [state] = useState({
    map: {
      center: [48.866667, 2.333333],
      zoom: 7,
    },
    locations: [
      {
        name: 'Mondial Relay Supermarche Speed',
        adress: '23 Rue Berthollet, 75005 Paris',
        position: [48.838510, 2.345760],
      },
      {
        name: 'Mondial Relay Les Independants',
        adress: '6 Rue du Moulin Joly, 75011 Paris',
        position: [48.868770, 2.379290],
      },
      {
        name: 'UPS Access Point',
        adress: 'Rue (Grande) 81, 72000 Le Mans',
        position: [48.029480, 0.139980],
      },
      {
        name: 'Relais Colis',
        adress: '7 Rue Professeur Gaffiot, 25000 Besançon',
        position: [47.234390, 5.974720],
      },
    ],
  });

  const getMarkers = useCallback(() => state.locations.map((location) => (
    <Marker position={location.position} icon={myIcon}>
      <Popup>
        { location.name }
        <br />
        { location.adress }
      </Popup>
    </Marker>
  )));

  return (
    <MapContainer center={state.map.center} zoom={state.map.zoom} scrollWheelZoom={false}>
      <TileLayer
        attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      {getMarkers()}
    </MapContainer>
  );
}

export default LeafletMap;
