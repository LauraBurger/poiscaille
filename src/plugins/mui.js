import { createTheme } from '@mui/material/styles';

const theme = createTheme({
  palette: {
    primary: {
      'white': '#ffffff',
      'amber': '#ffe575',
      'grey': '#eeeeee',
      'blue':'#00bcd4',
      'dark': '#424242',
    },
  },
});
